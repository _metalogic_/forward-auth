package server

//lint:file-ignore ST1001 dot import avoids package prefix in reference

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httputil"
	"strconv"
	"strings"

	"log/slog"

	authn "bitbucket.org/_metalogic_/authenticate"
	"bitbucket.org/_metalogic_/authorize/access"
	"bitbucket.org/_metalogic_/authorize/stores"
	. "bitbucket.org/_metalogic_/glib/http"
	"bitbucket.org/_metalogic_/glib/logging"
	"github.com/pborman/uuid"
)

var ok = []byte("ok")

// Auth authorizes a request based on configured access control rules;
// jwtHeader, traceHeader and userHeader are added to the forwarded request headers
func Auth(acs *access.System, userHeader, traceHeader string) func(w http.ResponseWriter, r *http.Request, params map[string]string) {

	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		if acs.Config.RunMode == "DISABLED" {
			slog.Warn("runMode is set to 'DISABLED' - all access controls are disabled")
			w.WriteHeader(http.StatusOK)
			w.Write(ok)
			return
		}

		testing := (r.Header.Get("Forward-Auth-Mode") == "testing")
		if testing {
			slog.Warn("authMode testing is enabled by Forward-Auth-Mode header - no requests are being forwarded")
		}

		if logging.Threshold(slog.Default()) == "DEBUG" {
			data, err := httputil.DumpRequest(r, false)
			if err != nil {
				ErrJSON(w, NewUnauthorizedError("authorization failed to unpack request"))
				return
			}
			raw := strconv.Quote(strings.ReplaceAll(strings.ReplaceAll(string(data), "\r", ""), "\n", "; "))
			slog.Debug(fmt.Sprintf("dump raw HTTP request: %s", raw[1:len(raw)-1]))
		}

		// pass or create traceID and add to request header
		traceID := r.Header.Get(traceHeader)
		if traceID == "" {
			traceID = uuid.New()
			slog.Debug("setting header", "header", traceHeader, "value", traceID)
			w.Header().Add(traceHeader, traceID)
		} else {
			slog.Debug("found header", "header", traceHeader, "value", traceID)
		}

		// forwarded request headers are used for authorization decisions
		group := r.Header.Get("X-Forwarded-Host")
		method := r.Header.Get("X-Forwarded-Method")
		uri := r.Header.Get("X-Forwarded-Uri")

		// allow all OPTIONS requests regardless of path;
		// we need this to avoid going mad allowing CORS preflight checks
		if method == http.MethodOptions {
			slog.Debug("allowing OPTIONS request")
			w.WriteHeader(http.StatusNoContent)
			return
		}

		// override checks on the request when a root bearer token is found
		// enabling root override avoids repeated use of "bearer('ROOT_KEY')" in check expressions
		if acs.Config.RootOverride && rootAuth(r, acs) {
			slog.Debug("allowing request with root bearer token")
			w.WriteHeader(http.StatusNoContent)
			return
		}

		// check for group overrides
		if acs.Override(group) == "allow" {
			if testing {
				tstJSON(w, http.StatusOK, "allow override for group "+group)
			} else {
				OkJSON(w, "allow override for group "+group)
			}
			slog.Debug("allow override for group " + group)
			return
		} else if acs.Override(group) == "deny" {
			if testing {
				tstJSON(w, http.StatusForbidden, "deny override for group "+group)
			} else {
				ErrJSON(w, NewForbiddenError("deny override for group "+group))
			}
			slog.Debug("deny override for group " + group)
			return
		}

		mux, err := acs.Muxer(group)
		if err != nil { // shouldn't happen
			ErrJSON(w, NewForbiddenError(err.Error()))
			return
		}

		// TODO get credentials from r.Header

		credentials := &authn.Credentials{}

		status, message := mux.Check(method, uri, credentials)

		if testing {
			tstJSON(w, status, message)
			return
		}

		switch status {
		case 401: // upstream should handle login
			ErrJSON(w, NewUnauthorizedError(message))
		case 403:
			ErrJSON(w, NewForbiddenError(message))
		case 404: // always deny on not found
			ErrJSON(w, NewForbiddenError(message))
		case 200:
			// TODO
			// if username != "" {
			// 	slog.Debug("Adding HTTP header %s %s", userHeader, username)
			// 	w.Header().Add(userHeader, username)
			// }
			w.Write(ok)
		}
	}
}

// Tree returns a text representation of the access tree
func Tree(acs *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		data, err := json.MarshalIndent(acs, "", "  ")
		if err != nil {
			ErrJSON(w, err)
		}
		MsgJSON(w, string(data))
	}
}

// Update forces an auth update from a store (invoked via broadcast from /reload)
func Update(acs *access.System, store stores.Loader) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		// get new access control system from the store
		var err error
		acs, err = store.Load(context.Background(), acs.Config)
		if err != nil {
			ErrJSON(w, err)
			return
		}
		// update access
		err = acs.UpdateFunc()(acs)
		if err != nil {
			ErrJSON(w, err)
			return
		}
		MsgJSON(w, "access system update succeeded")
	}
}

func rootAuth(r *http.Request, acs *access.System) bool {
	authHeader := r.Header.Get("Authorization")

	var token string
	if authHeader != "" {
		// Get the Bearer auth token
		splitToken := strings.Split(authHeader, "Bearer ")
		if len(splitToken) == 2 {
			token = splitToken[1]
		}
	}

	// allow all requests with ROOT_KEY
	return access.CheckBearerAuth(acs, token, []string{"ROOT_KEY"}...)
}
