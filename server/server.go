package server

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"sync"

	"bitbucket.org/_metalogic_/access-apis/client"
	authn "bitbucket.org/_metalogic_/authenticate"
	"bitbucket.org/_metalogic_/authorize/access"
	"bitbucket.org/_metalogic_/authorize/stores"
	"bitbucket.org/_metalogic_/config"
	"github.com/dimfeld/httptreemux/v5"
)

const rootGUID = "ROOT"

var (
	// sessionName        string
	// sessionMode        bool
	accessRootURL      string
	accessTenantID     string
	accessAPIKey       string
	insecureSkipVerify bool
	maxUploadSize      int64
	sessionConfig      *SessionConfig
)

type SessionConfig struct {
	Enable bool
	Mode   string
	Name   string
}

// AuthzServer ...
type AuthzServer struct {
	server *http.Server
	client *client.Client
	store  stores.Loader
	info   map[string]string
}

func Start(addr, tenantParam, jwtHeader, userHeader, traceHeader string, acs *access.System, client *client.Client, store stores.Loader, wg *sync.WaitGroup) (svr *AuthzServer) {

	// default to local container for access-apis
	accessRootURL = config.IfGetenv("ACCESS_APIS_ROOT_URL", "http://access-apis-service.metalogic.svc.cluster.local:8080")
	accessTenantID = config.IfGetenv("ACCESS_APIS_TENANT_ID", "UNDEFINED")
	accessAPIKey = config.IfGetenv("ACCESS_APIS_TENANT_KEY", "UNDEFINED")
	insecureSkipVerify = config.IfGetBool("INSECURE_SKIP_VERIFY", false)

	sessionConfig = &SessionConfig{
		Enable: config.IfGetBool("SESSION_ENABLE", true),
		Mode:   config.IfGetenv("SESSION_MODE", "COOKIE"),
		Name:   config.IfGetenv("SESSION_NAME", "Session"),
	}

	// public key must be available either by HTTP request or in the environment
	url := config.IfGetenv("IDENTITY_PROVIDER_PUBLIC_KEY_URL", "")

	var err error
	var publicKey []byte
	if url != "" {
		publicKey, err = getPublicKey(url)
		if err != nil {
			slog.Error(err.Error())
			os.Exit(1)
		}
	} else {
		publicKey = []byte(config.MustGetConfig("IDENTITY_PROVIDER_PUBLIC_KEY"))
	}

	// Symmetric secret key
	secretKey := []byte(config.MustGetConfig("JWT_SECRET_KEY"))

	authn.Init(publicKey, secretKey)

	// TODO jwtRefreshKey := []byte(config.MustGetConfig("JWT_REFRESH_SECRET_KEY"))

	// auth := fauth.NewAuth(addr)
	svr = &AuthzServer{
		server: &http.Server{
			Addr:    addr,
			Handler: router(acs, client, store, userHeader, traceHeader)},
		store: store,
		info:  make(map[string]string),
	}

	slog.Debug("configured authorization environment", "server", svr)

	// start the HTTP server
	go func() {
		defer wg.Done() // let main know we are done cleaning up

		// always returns ErrServerClosed on graceful close
		if err := svr.server.ListenAndServe(); err != http.ErrServerClosed {
			// unexpected error. port in use?
			slog.Error("ListenAndServe() exited", "error", err)
			os.Exit(1)
		}
	}()

	// returning reference so caller can call Shutdown()
	return svr
}

// Stats returns server statistics
// TODO keep running stats of authorization request handling
func (svc *AuthzServer) Stats() string {
	js := fmt.Sprintf("{\"Requests\": %d, \"Allowed\" : %d, \"Denied\": %d}", 100, 50, 50)
	return js
}

// Shutdown does a clean shutdown of the authorization server
func (svc *AuthzServer) Shutdown(ctx context.Context) {
	svc.store.Close()
	if err := svc.server.Shutdown(ctx); err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}
	slog.Warn("shutdown Authz server")
}

// TODO get headers from environment
func optionsHandler(w http.ResponseWriter, r *http.Request, params map[string]string) {
	// Implement your logic here to handle OPTIONS requests
	// For example, you can set CORS headers or handle preflight requests

	// Set CORS headers to allow requests from any origin (you may modify this based on your requirements)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, Cookie")

	// Respond to the preflight request with a 204 No Content status
	// TODO no need to check - we must be handling an OPTIONS request
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusNoContent)
		return
	}
}

// create the router for Service
func router(acs *access.System, client *client.Client, loader stores.Loader, userHeader, traceHeader string) *httptreemux.TreeMux {
	// initialize HTTP router;
	// forward-auth expects to be deployed at the root of a unique host (eg auth.example.com)

	allowOrigin := config.IfGetenv("CORS_ALLOW_ORIGIN", "*")
	allowHeaders := config.IfGetenv("CORS_ALLOW_HEADERS", "*")

	corsFunc := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", allowOrigin)
			w.Header().Set("Access-Control-Allow-Headers", allowHeaders)
			w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, HEAD, OPTIONS")
			w.Header().Add("Access-Control-Allow-Credentials", "true")

			next.ServeHTTP(w, r)
		})
	}

	treemux := httptreemux.New()

	treemux.OptionsHandler = optionsHandler

	api := treemux.NewGroup("/")
	api.UseHandler(corsFunc)

	// Common endpoints

	healthChecker := loader.(stores.HealthChecker)
	api.GET("/health", Health(healthChecker))

	infoProvider := loader.(stores.InfoProvider)
	api.GET("/info", APIInfo(infoProvider))

	statsProvider := loader.(stores.StatsProvider)
	api.GET("/stats", Stats(statsProvider))

	// Admin endpoints
	api.GET("/admin/loglevel", LogLevel())
	api.PUT("/admin/loglevel/:verbosity", SetLogLevel())
	api.GET("/admin/run", RunMode())
	api.PUT("/admin/run/:mode", SetRunMode())
	api.GET("/admin/tree", Tree(acs))

	// Auth endpoints
	api.GET("/auth", Auth(acs, userHeader, traceHeader))
	api.POST("/auth/update", Update(acs, loader)) // called by deployment-api broadcast to trigger update from loader

	// Session endpoints
	api.POST("/login", Login(acs))
	api.PUT("/logout", Logout(acs))
	api.PUT("/refresh", Refresh(acs))
	api.GET("/sessions", Sessions(acs))
	api.GET("/sessions/:sid", Session(acs))
	api.GET("/blocks", Blocked(acs))
	api.PUT("/blocks/:uid", Block(acs))
	api.DELETE("/blocks/:uid", Unblock(acs))

	// User endpoints
	api.GET("/users", Users(acs, client))
	api.POST("/users", CreateUser(acs, client))
	api.GET("/users/:uid", User(acs, client))
	api.PUT("/users/:uid", UpdateUser(acs, client))
	api.DELETE("/users/:uid", DeleteUser(acs, client))

	api.POST("/users/:uid/password", ChangePassword(acs, client))
	api.PUT("/users/:uid/password", SetPassword(acs, client))
	api.POST("/password-reset", StartPasswordReset(acs, client))

	// Context endpoints
	api.GET("/contexts", Contexts(acs, client))
	api.POST("/contexts", CreateContext(acs, client))
	api.GET("/contexts/:uid", Context(acs, client))
	api.PUT("/contexts/:uid", UpdateContext(acs, client))
	api.DELETE("/contexts/:uid", DeleteContext(acs, client))

	// ACS endpoints - the file storage adapter does not implement these endpoints
	// api.GET("/hostgroups", HostGroups(loader))
	// api.POST("/hostgroups", CreateHostGroup(userHeader, loader))
	// api.GET("/hostgroups/:gid", HostGroup(loader))
	// api.PUT("/hostgroups/:gid", UpdateHostGroup(userHeader, loader))
	// api.DELETE("/hostgroups/:gid", DeleteHostGroup(loader))

	// api.GET("/hostgroups/:gid/hosts", Hosts(loader))
	// api.POST("/hostgroups/:gid/hosts", CreateHost(userHeader, loader))
	// api.GET("/hostgroups/:gid/hosts/:hid", Host(loader))
	// api.PUT("/hostgroups/:gid/hosts/:hid", UpdateHost(userHeader, loader))
	// api.DELETE("/hostgroups/:gid/hosts/:hid", DeleteHost(loader))

	// api.GET("/hostgroups/:gid/checks", Checks(loader))
	// api.POST("/hostgroups/:gid/checks", CreateCheck(userHeader, loader))
	// api.GET("/hostgroups/:gid/checks/:ckid", Check(loader))
	// api.PUT("/hostgroups/:gid/checks/:ckid", UpdateCheck(userHeader, loader))
	// api.DELETE("/hostgroups/:gid/checks/:ckid", DeleteCheck(loader))

	// api.GET("/hostgroups/:gid/checks/:ckid/paths", Paths(loader))
	// api.POST("/hostgroups/:gid/checks/:ckid/paths", CreatePath(userHeader, loader))
	// api.GET("/hostgroups/:gid/checks/:ckid/paths/:pid", Path(loader))
	// api.PUT("/hostgroups/:gid/checks/:ckid/paths/:pid", UpdatePath(userHeader, loader))
	// api.DELETE("/hostgroups/:gid/checks/:ckid/paths/:pid", DeletePath(loader))

	return treemux
}

func getPublicKey(url string) (publicKey []byte, err error) {
	slog.Debug("getting RSA public key", "url", url)
	client := &http.Client{}

	if insecureSkipVerify {
		slog.Warn("InsecureSkipVerify is enabled for http.Client - DO NOT DO THIS IN PRODUCTION")
		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}

	r, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return publicKey, fmt.Errorf("error creating request GET %s: %s", url, err)
	}

	resp, err := client.Do(r)
	if err != nil {
		return publicKey, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return publicKey, fmt.Errorf(resp.Status)
	}

	publicKey, err = io.ReadAll(resp.Body)
	if err != nil {
		return publicKey, err
	}

	return publicKey, nil
}

// SetMaxUploadSize ...
func SetMaxUploadSize(sizeInBytes int64) {
	maxUploadSize = sizeInBytes
}

func getMaxUploadSize() int64 {
	return maxUploadSize
}
