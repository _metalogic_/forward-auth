package server

//lint:file-ignore ST1001 dot import avoids package prefix in reference

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"session"
	"strings"
	"time"

	"bitbucket.org/_metalogic_/access-apis/client"
	authn "bitbucket.org/_metalogic_/authenticate"
	"bitbucket.org/_metalogic_/authorize/access"
	"bitbucket.org/_metalogic_/config"
	. "bitbucket.org/_metalogic_/glib/http"
	"bitbucket.org/_metalogic_/log"
)

// Login executes a user login against the access-api
func Login(acs *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		token := access.Bearer(r)
		if token == "" {
			ErrJSON(w, NewBadRequestError("login requires a valid application bearer token"))
			return
		}

		decoder := json.NewDecoder(r.Body)

		type Login struct {
			Email    string `json:"email"`
			Password string `json:"password"`
		}

		login := &Login{}

		// unmarshal JSON into &Login
		err := decoder.Decode(login)
		if err != nil {
			ErrJSON(w, NewServerError(err.Error()))
			return
		}

		c, err := client.New(accessRootURL, accessTenantID, accessAPIKey, true, slog.Default())
		if err != nil {
			ErrJSON(w, NewServerError("new access-apis client failed: "+err.Error()))
			return
		}

		auth, err := c.Login(login.Email, login.Password)
		if err != nil {
			ErrJSON(w, NewUnauthorizedError(fmt.Sprintf("user login failed for %s: ", login.Email)))
			return
		}

		user, err := authn.NewUser(auth.JWT.Token)
		// identity, err := acs.JWTIdentity(auth.JWT.JWTToken)
		if err != nil {
			ErrJSON(w, NewServerError("failed to parse identity from login response: "+err.Error()))
			return
		}

		data, err := json.Marshal(user)
		if err != nil {
			ErrJSON(w, NewServerError("shouldn't: failed to marshal user: "+err.Error()))
			return
		}

		if sessionConfig == nil {
			ErrJSON(w, NewServerError("session is not configured"))
			return
		}

		sessionID, expiresAt := session.NewSession(token, auth.JWT, false)

		setSessionID(w, sessionConfig.Mode, sessionConfig.Name, sessionID, expiresAt)

		log.Debugf("response headers: %+v", w.Header())

		OkJSON(w, string(data))
	}
}

// Logout executes a logout for the attached session token
func Logout(acs *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		app := access.Bearer(r)
		if app == "" {
			ErrJSON(w, NewBadRequestError("logout requires a valid application bearer token"))
			return
		}

		if id, err := invalidateSessionID(w, r, sessionConfig.Mode, sessionConfig.Name); err != nil {
			ErrJSON(w, fmt.Errorf("error logging out session id %s: %s", id, err))
			return
		} else {
			session.DeleteSession(app, id)
			MsgJSON(w, fmt.Sprintf("logged out session with id %s", id))
		}
	}
}

// Refresh executes a refresh request against the access-apis with the session refreshToken
func Refresh(acs *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		token := access.Bearer(r)
		if token == "" {
			ErrJSON(w, NewBadRequestError("refresh requires a valid application bearer token"))
			return
		}

		id, err := sessionID(r, sessionConfig.Mode, sessionConfig.Name)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		sess, err := session.AppSession(token, id)
		if err != nil {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("session id '%s' not found", id)))
			return
		}

		if sess.IsExpired() {
			ErrJSON(w, NewUnauthorizedError("session is expired"))
			return
		}

		c, err := client.New(accessRootURL, accessTenantID, accessAPIKey, true, slog.Default())
		if err != nil {
			ErrJSON(w, NewServerError("failed to create access-apis client: "+err.Error()))
			return
		}

		auth, err := c.Refresh(sess.ID, sess.JWTRefresh)
		if err != nil {
			ErrJSON(w, NewUnauthorizedError(fmt.Sprintf("refresh failed for UID %s", sess.ID)))
			return
		}

		user, err := authn.NewUser(auth.JWT.Token)
		if err != nil {
			ErrJSON(w, NewServerError("failed to parse identity from login response: "+err.Error()))
			return
		}

		if user.ID() != sess.ID {
			ErrJSON(w, NewServerError("shouldn't: JWT user differs from session user"))
			return
		}

		data, err := json.Marshal(user.Identity)
		if err != nil {
			ErrJSON(w, NewServerError("shouldn't: failed to marshal identity: "+err.Error()))
			return
		}

		expiresAt := session.UpdateSession(id, token, auth.JWT)

		setSessionID(w, sessionConfig.Mode, sessionConfig.Name, id, expiresAt)

		OkJSON(w, string(data))
	}
}

// Sessions returns a JSON array of active applications sessions
func Sessions(svc *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		token := access.Bearer(r)
		if token == "" {
			ErrJSON(w, NewBadRequestError("sessions requires a valid application bearer token"))
			return
		}

		OkJSON(w, session.AppSessions(token))
	}
}

// Session gets the session for a given session ID
func Session(svc *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		token := access.Bearer(r)
		if token == "" {
			ErrJSON(w, NewBadRequestError("refresh session a valid application bearer token"))
			return
		}

		sid := params["sid"]
		sess, err := session.AppSession(token, sid)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		// TODO allow query param for getting expired sessions

		if sess.IsExpired() {
			ErrJSON(w, NewBadRequestError("session is expired"))
			return
		}

		// set session cookie
		setSessionID(w, sessionConfig.Mode, sessionConfig.Name, sid, sess.ExpiresAt())

		log.Debugf("response headers: %+v", w.Header())

		// return session identity JSON in response
		OkJSON(w, sess.JSON())

	}
}

// Blocked returns an array of blocked users
func Blocked(auth *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		msgJSONList(w, auth.Blocked())
	}
}

// Block adds uid to the user blocklist
func Block(svc *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		uid := params["uid"]
		svc.Block(uid)
		b := fmt.Sprintf("{ \"blocked\" : \"%s\" }", uid)
		MsgJSON(w, b)
	}
}

// Unblock removes the used with uid from the blocklist
func Unblock(svc *access.System) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		uid := params["uid"]
		svc.Unblock(uid)
		b := fmt.Sprintf("{ \"unblocked\" : \"%s\" }", uid)
		MsgJSON(w, b)
	}
}

func sessionID(r *http.Request, sessionMode, sessionName string) (id string, err error) {
	switch strings.ToLower(sessionMode) {
	case "cookie":
		if cookie, err := r.Cookie(sessionName); err != nil {
			return id, err
		} else if cookie == nil {
			return id, fmt.Errorf("session cookie not found with name %s", sessionName)
		} else {
			return cookie.Name, nil
		}
	case "header":
		id := r.Header.Get(sessionName)
		if id == "" {
			return id, fmt.Errorf("session header not found with name %s", sessionName)
		}
		return id, nil
	default:
		return id, fmt.Errorf("invalid session mode %s", sessionMode)
	}
}

func setSessionID(w http.ResponseWriter, sessionMode, sessionName, sessionID string, expiresAt time.Time) (err error) {

	switch strings.ToLower(sessionMode) {
	case "cookie":
		httpOnly := config.IfGetBool("SESSION_HTTP_ONLY_COOKIE", false)
		secure := config.IfGetBool("SESSION_SECURE_COOKIE", true)
		cookie := http.Cookie{
			Name:  sessionName,
			Value: sessionID,
			// for debugging from localhost	Domain:   cookieDomain,
			HttpOnly: httpOnly,
			Secure:   secure,
			Expires:  expiresAt,
			SameSite: http.SameSiteNoneMode,
		}

		log.Debugf("setting session cookie: %+v", cookie)

		// set session cookie in response and return user identity JSON
		http.SetCookie(w, &cookie)
		return nil
	case "header":
		cookie := http.Cookie{
			Value:   sessionID,
			Expires: expiresAt,
		}
		w.Header().Set(sessionName, cookie.String())
		return nil
	default:
		return fmt.Errorf("invalid session mode: %s", sessionMode)
	}

}

func invalidateSessionID(w http.ResponseWriter, r *http.Request, sessionMode, sessionName string) (id string, err error) {

	switch strings.ToLower(sessionMode) {
	case "cookie", "header":
		httpOnly := config.IfGetBool("SESSION_HTTP_ONLY_COOKIE", false)
		secure := config.IfGetBool("SESSION_SECURE_COOKIE", true)
		cookieDomain := config.IfGetenv("SESSION_COOKIE_DOMAIN", "")

		cookie, err := r.Cookie(sessionName)

		if err == nil {
			id = cookie.Value

		}
		expired := &http.Cookie{
			Name:     sessionName,
			Domain:   cookieDomain,
			Expires:  time.Unix(0, 0),
			HttpOnly: httpOnly,
			Secure:   secure,
		}
		// set expired session cookie in response and return user identity JSON
		http.SetCookie(w, expired)
		return id, nil

	// case "header":
	// 	id := r.Header.Get(sessionName)
	// 	if id == "" {
	// 		return id, fmt.Errorf("session header not found with name %s", sessionName)
	// 	}
	// 	// set empty header
	// 	return "", nil
	default:
		return id, fmt.Errorf("invalid session mode %s", sessionMode)
	}

}

func setResetToken(w http.ResponseWriter, sessionMode, sessionName, sessionID string, expiresAt time.Time) (err error) {

	switch strings.ToLower(sessionMode) {
	case "cookie", "header":
		httpOnly := config.IfGetBool("SESSION_HTTP_ONLY_COOKIE", false)
		secure := config.IfGetBool("SESSION_SECURE_COOKIE", true)
		cookie := http.Cookie{
			Name:  sessionName,
			Value: sessionID,
			// for debugging from localhost	Domain:   cookieDomain,
			HttpOnly: httpOnly,
			Secure:   secure,
			Expires:  expiresAt,
			SameSite: http.SameSiteNoneMode,
		}

		log.Debugf("setting session cookie: %+v", cookie)

		// set session cookie in response and return user identity JSON
		http.SetCookie(w, &cookie)
		return nil
	// case "header":
	// 	cookie := http.Cookie{
	// 		Value:   sessionID,
	// 		Expires: expiresAt,
	// 	}
	// 	w.Header().Set(sessionName, cookie.String())
	// 	return nil
	default:
		return fmt.Errorf("invalid session mode: %s", sessionMode)
	}

}

// Changepassword changes user password
func ChangePassword(svc *access.System, client *client.Client) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		uid := params["uid"]

		err := client.ChangePasswordRaw(uid, r.Body)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		NoContent(w)
	}
}

// SetPassword sets user password
func SetPassword(svc *access.System, client *client.Client) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		uid := params["uid"]

		err := client.SetPasswordRaw(uid, r.Body)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		NoContent(w)
	}
}

// StartPasswordReset initiates a password reset flow
func StartPasswordReset(acs *access.System, client *client.Client) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		auth, err := client.StartPasswordResetRaw(r.Body)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		user, err := authn.NewUser(auth.JWT.Token)
		if err != nil {
			ErrJSON(w, NewServerError("failed to obtain user from JWT"))
			return
		}

		token := access.Bearer(r)

		// id is a 6 digit string emailed to the user
		id, expiresAt := session.NewSession(token, auth.JWT, true)

		// send reset code to user email by client mailer
		if err = client.Sendmail(user.Name(), user.Email(), "Password Reset Code", fmt.Sprintf("Reset Code: %s expiring at %s", id, expiresAt)); err != nil {
			ErrJSON(w, err)
			return
		}

		type resetResponse struct {
			UID   string `json:"uid"`
			Email string `json:"email"`
			// Expiry time.Time `json:"expiry"`
		}

		reset := &resetResponse{
			UID:   user.ID(),
			Email: user.Email(),
			// Expiry: *ident.ExpiresAt,
		}

		resetJSON, err := json.Marshal(reset)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		OkJSON(w, string(resetJSON))
	}
}

// ResetPassword recovers a user account by reseting user password
func ResetPassword(svc *access.System, client *client.Client) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		uid := params["uid"]

		var message []byte

		message, err := client.ResetPasswordRaw(uid, r.Body)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		MsgJSON(w, string(message))
	}
}
