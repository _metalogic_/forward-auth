package server

//lint:file-ignore ST1001 dot import avoids package prefix in reference

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/_metalogic_/authorize/stores"
	"bitbucket.org/_metalogic_/build"
	. "bitbucket.org/_metalogic_/glib/http"
	_ "bitbucket.org/_metalogic_/glib/types"
	"bitbucket.org/_metalogic_/log"
)

var runMode string

func init() {
	runMode = "enforcing"
}

// APIInfo returns name and version version info
func APIInfo(store stores.InfoProvider) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		rt := &build.Runtime{
			BuildInfo:   build.Info,
			ServiceInfo: store.Info(),
			LogLevel:    log.GetLevel().String(),
		}

		runtimeJSON, err := json.Marshal(rt)
		if err != nil {
			ErrJSON(w, NewServerError(err.Error()))
			return
		}
		OkJSON(w, string(runtimeJSON))
	}
}

// Health returns ok if the service is healthy
func Health(store stores.HealthChecker) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "text/plain")
		err := store.Health()
		if err != nil {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable)+fmt.Sprintf(": %s", err), http.StatusServiceUnavailable)
			return
		}
		fmt.Fprint(w, "ok\n")
		return
	}
}

// @Tags Common endpoints
// @Summary get forward-auth service statistics
// @Description get forward-auth service statistics, currently database stats only
// @ID get-stats
// @Produce  json
// @Success 200 {object} fauth.Stats
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /forward-auth/v1/stats [get]
func Stats(store stores.StatsProvider) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		stats := store.Stats()
		fmt.Fprint(w, stats)
		return
	}
}

// @Tags Admin endpoints
// @Summary gets the current service log level
// @Description gets the service log level (one of Trace, Debug, Info, Warn or Error)
// @ID get-loglevel
// @Produce json
// @Success 200 {object} types.Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /forward-auth/v1/admin/loglevel [get]
func LogLevel() func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		MsgJSON(w, log.GetLevel().String())
	}
}

// @Tags Admin endpoints
// @Summary sets the service log level
// @Description dynamically sets the service log level to one of Trace, Debug, Info, Warn or Error
// @ID set-loglevel
// @Produce json
// @Param verbosity path string true "Log Level"
// @Success 200 {object} types.Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /forward-auth/v1/admin/loglevel [put]
func SetLogLevel() func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		var (
			verbosity string
		)

		verbosity = params["verbosity"]
		verbosity = strings.ToLower(verbosity)

		var v log.Level
		switch verbosity {
		case "error":
			v = log.ErrorLevel
		case "info":
			v = log.InfoLevel
		case "debug":
			v = log.DebugLevel
		case "trace":
			v = log.TraceLevel
		default:
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid log level: %s", verbosity)))
			return
		}

		log.SetLevel(v)
		MsgJSON(w, v.String())
	}
}

// @Summary gets the mode for authorization, currently either enforcing or none
// @Description sets the mode for authorization, currently either enforcing or none
// @ID set-runmode
// @Produce json
// @Param verbosity path string true "Log Level"
// @Success 200 {object} types.Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
func RunMode() func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		MsgJSON(w, fmt.Sprintf("run mode is set to '%s'", runMode))
	}
}

// @Tags Admin endpoints
// @Summary sets the run mode for authorization, currently either enforcing or none
// @Description sets the run mode for authorization, currently either enforcing or none
// @ID set-runmode
// @Produce json
// @Param verbosity path string true "Log Level"
// @Success 200 {object} types.Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
func SetRunMode() func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		mode := params["mode"]
		runMode = strings.ToLower(mode)
		MsgJSON(w, fmt.Sprintf("run mode is set to '%s'", runMode))
	}
}
