// package globals defines global constants and variables dot-imported throughout the application

package globals

const (
	// these should agree with base.CONTACT_CHANNELS
	CHANNEL_EMAIL = "EMAIL"
	CHANNEL_SLACK = "SLACK"

	// not yet supported
	CHANNEL_SMS      = "SMS"
	CHANNEL_WHATSAPP = "WHATSAPP"
)
