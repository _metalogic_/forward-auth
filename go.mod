module bitbucket.org/_metalogic_/forward-auth

go 1.23

require (
	bitbucket.org/_metalogic_/access-apis v1.3.8
	bitbucket.org/_metalogic_/authenticate v0.8.1
	bitbucket.org/_metalogic_/authorize v0.8.2-0.20241021221529-0e953a61d148
	bitbucket.org/_metalogic_/build v1.0.2
	bitbucket.org/_metalogic_/config v1.5.0
	bitbucket.org/_metalogic_/glib v1.7.1
	bitbucket.org/_metalogic_/log v1.7.0
	github.com/dimfeld/httptreemux/v5 v5.5.0
	github.com/pborman/uuid v1.2.0
	github.com/sendgrid/sendgrid-go v3.13.0+incompatible
	github.com/swaggo/swag v1.16.2
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/env v0.8.0 // indirect
	bitbucket.org/_metalogic_/eval v1.3.0 // indirect
	bitbucket.org/_metalogic_/httpsig v0.0.0-20220629165111-0f9208377a16 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	bitbucket.org/_metalogic_/senders v0.0.0-20231130171936-ec4dac07fb01 // indirect
	bitbucket.org/_metalogic_/validation v0.0.0-20210601150612-ed58a6ac8ba6 // indirect
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/denisenkom/go-mssqldb v0.12.3 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-chi/chi v4.0.0+incompatible // indirect
	github.com/go-openapi/jsonpointer v0.20.0 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.9 // indirect
	github.com/go-openapi/swag v0.22.4 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mailgun/mailgun-go/v3 v3.6.4 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sendgrid/rest v2.6.9+incompatible // indirect
	golang.org/x/crypto v0.15.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/tools v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
