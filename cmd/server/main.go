package main

import (
	"context"
	"flag"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/_metalogic_/access-apis/client"
	"bitbucket.org/_metalogic_/authorize/access"
	"bitbucket.org/_metalogic_/authorize/stores"
	"bitbucket.org/_metalogic_/authorize/stores/file"
	"bitbucket.org/_metalogic_/authorize/stores/mssql"
	"bitbucket.org/_metalogic_/authorize/stores/postgres"
	"bitbucket.org/_metalogic_/build"
	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/forward-auth/server"
	"bitbucket.org/_metalogic_/glib/http"
)

var (
	info       build.BuildInfo
	configFlg  string
	disableFlg bool
	levelFlg   string
	portFlg    string
	storageFlg string

	accessClient *client.Client
)

func init() {

	flag.StringVar(&configFlg, "config", "", "path to file adapter config directory")
	flag.BoolVar(&disableFlg, "disable", false, "disable forward authorization")
	flag.StringVar(&levelFlg, "level", "INFO", "set log level to one of debug, info, warning, error")
	flag.StringVar(&portFlg, "port", ":8080", "HTTP listen port")
	flag.StringVar(&storageFlg, "store", config.IfGetenv("FORWARD_AUTH_STORAGE", "file"), "storage adapter type - one of file, mssql, mock")

	var err error
	info = build.Info

	http.Module = info.Project

	version := info.String()
	command := info.Name()

	flag.Usage = func() {
		fmt.Printf("Project %s:\n\n", version)
		fmt.Printf("Usage: %s -help (this message) | %s [options]:\n\n", command, command)
		flag.PrintDefaults()
	}

	accessURL := config.IfGetenv("ACCESS_APIS_ROOT_URL", "https://apis.auth9.net")
	accessClient, err = client.New(accessURL, config.MustGetenv("ACCESS_APIS_TENANT_ID"), config.MustGetenv("ACCESS_APIS_TENANT_KEY"), config.IfGetBool("INSECURE_SKIP_VERIFY", true), slog.Default())
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}

}

func main() {
	flag.Parse()

	server.SetMaxUploadSize(int64(config.IfGetInt("MAX_UPLOAD_BYTES", 15000000)))

	// - rootToken is the name of the tenant API token that is treated as ROOT
	// - tenantParam is the name of the tenant ID path parameter used in rule expressions
	// - jwtHeader is the name of the header in requests that carries a user JSON Web Token
	// - userHeader is the name of the header containing the user identifier extracted from the JWT
	// - traceHeader is the name of the header containing a trace identifier used for log coordination
	//   and returned by forward-auth; Traefik attaches the userHeader and traceHeader to the request
	//   for downstream consumption

	tenantParam := config.IfGetenv("TENANT_PARAM_NAME", ":tenantID")
	jwtHeader := config.IfGetenv("JWT_HEADER_NAME", "X-Jwt-Header")
	userHeader := config.IfGetenv("USER_HEADER_NAME", "X-User-Header")
	traceHeader := config.IfGetenv("TRACE_HEADER_NAME", "X-Trace-Header")

	var logger *slog.Logger

	loglevel := os.Getenv("LOG_LEVEL")
	if loglevel == "DEBUG" {
		logger = slog.New(slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{Level: slog.LevelDebug}))
	}

	slog.SetDefault(logger)
	accessClient.SetLogger(logger)

	var store stores.Loader
	var err error
	switch storageFlg {
	case "file":
		dataDir := configFlg
		if dataDir == "" {
			dataDir = config.IfGetenv("FORWARD_AUTH_DATA_DIR", "/usr/local/etc/forward-auth")
		}
		store, err = file.New(dataDir)
	case "mssql":
		store, err = mssql.New()
	case "postgres":
		store, err = postgres.New()
	}

	if err != nil {
		slog.Error("failed to create forward-auth service", "type", store.ID(), "error", err)
		os.Exit(1)
	}

	defer store.Close()

	exitDone := &sync.WaitGroup{}
	exitDone.Add(1)

	var runMode string
	config.IfGetenv("RUN_MODE", "ENFORCING")
	if disableFlg {
		runMode = "DISABLED"
	}

	conf := &access.Config{
		RunMode:      runMode,
		RootOverride: config.IfGetBool("ROOT_OVERRIDE", true),
		SessionMode:  config.IfGetBool("SESSION_MODE", true),
		SessionName:  config.IfGetenv("SESSION_NAME", ""),
	}

	slog.Debug("access system configuration", "config", conf)

	// load the access controls
	slog.Debug("loading access system from storage", "adapter", store.ID())

	acs, err := store.Load(context.Background(), conf)
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}

	authzSrv := server.Start(portFlg, tenantParam, jwtHeader, userHeader, traceHeader, acs, accessClient, store, exitDone)

	slog.Info("forward-auth authorization server started")

	// Wait for a SIGINT (perhaps triggered by user with CTRL-C) or SIGTERM (from Docker)
	// Run cleanup when signal is received
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	<-stop

	slog.Warn("forward-auth received stop signal - shutting down")

	// now close the servers gracefully ("shutdown")
	var ctx context.Context
	var cancel context.CancelFunc

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	slog.Warn("stopping authorization server")
	authzSrv.Shutdown(ctx)

	// wait for goroutines started in StartEventServer() to stop
	exitDone.Wait()

	slog.Warn("forward-auth shutdown complete - exiting")

}
